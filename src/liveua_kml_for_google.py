# Most of this file is dedicated to creating KML that is usable in a Google Map.
# The Google KML styles seem to follow no standard, so they are just reverse engineered

from datetime import datetime
from timezonefinder import TimezoneFinder
from xml.dom import minidom
import fileinput
import json
import pytz
import sys
import xml.etree.ElementTree as ET

import google_icons as GI
import kml_helpers as KH


# Keep these out of the result
SKIP_EXTENDED_DATA = ['color_id','id','marker-color','marker-size','marker-symbol','picpath','resource','timestamp']

# Add styles and style map
def add_styles(out_document, icon_symbol, icon_color, placemark_count):
  add_style(out_document, icon_symbol, icon_color, placemark_count, 'normal', '0')
  add_style(out_document, icon_symbol, icon_color, placemark_count, 'highlight', '1')
  style_map = ET.SubElement(out_document, 'StyleMap')
  style_map.attrib["id"] = 'icon-%s-%s-%d' % (icon_symbol, icon_color, placemark_count)
  pair = ET.SubElement(style_map, 'Pair')
  ET.SubElement(pair,'key').text = 'normal'
  ET.SubElement(pair,'styleUrl').text = '#icon-%s-%s-normal-%d' % (icon_symbol, icon_color, placemark_count)
  pair = ET.SubElement(style_map, 'Pair')
  ET.SubElement(pair,'key').text = 'highlight'
  ET.SubElement(pair,'styleUrl').text = '#icon-%s-%s-highlight-%d' % (icon_symbol, icon_color, placemark_count)


# Add individual style
def add_style(out_document, icon_symbol, icon_color, placemark_count, style_type, label_scale):
  out_style = ET.SubElement(out_document, 'Style')
  out_style.attrib['id'] = 'icon-%s-%s-%s-%d' % (icon_symbol, icon_color, style_type, placemark_count)
  icon_style = ET.SubElement(out_style, 'IconStyle')
  ET.SubElement(icon_style, 'color').text = 'ff%s' % icon_color.lower()
  ET.SubElement(icon_style, 'scale').text = '1'
  KH.add_with_child(icon_style, 'Icon', 'href', 'https://www.gstatic.com/mapspro/images/stock/503-wht-blank_maps.png')
  KH.add_with_child(out_style, 'LabelStyle', 'scale', label_scale)


def get_placemark_name(feature):
  return 'Live UA Report' # TODO something smarter Make this specific to the type of event

# Prints a Google Map compatible KML file to STDOUT
def parseGeoJSON(json_string):
  out_kml = ET.Element('kml')
  out_kml.attrib['xmlns'] = 'http://www.opengis.net/kml/2.2'
  out_document = ET.SubElement(out_kml, 'Document')
  ET.SubElement(out_document, 'name').text = 'liveuamap import'

  # Styles need to be uniquily named, so we iterate for each Placemark
  placemark_count = 0

  geo_data = json.loads(json_string)

  if not 'features' in geo_data:
    sys.exit('no features in json')

  # Iterate over all placemarks and add them to the output
  for feature in geo_data['features']:
    color = pics = picture = coordinates = date = None
    placemark_count += 1
    if 'geometry' in feature:
      if feature['geometry']['type'] == 'Point':
        coords = feature['geometry']['coordinates']
        coordinates = ','.join(map(str, coords))
        tf = TimezoneFinder()
        tz_str = tf.timezone_at(lng=coords[0], lat=coords[1])
        tz = pytz.timezone(tz_str)

        props = feature['properties']
        if 'marker-color' in props:
          color = props['marker-color']
        if 'pics' in props and len(props['pics']) > 0:
          pics = ' '.join(map(str, props['pics']))
        if 'picture' in props and props['picture']:
          picture = props['picture']
        if 'timestamp' in props:
          date = datetime.fromtimestamp(props['timestamp'], tz=tz).isoformat()

    # If we have coordinates we have enough to add to the output
    if (coordinates is not None):
      icon_symbol = GI.icon_symbol_of_json(feature)
      icon_color = GI.convert_color(color)
      add_styles(out_document, icon_symbol, icon_color, placemark_count)
      out_placemark = ET.SubElement(out_document,'Placemark')
      ET.SubElement(out_placemark,'name').text = get_placemark_name(feature)
      ET.SubElement(out_placemark,'styleUrl').text = "#icon-%s-%s-%d" % (icon_symbol, icon_color, placemark_count)
      KH.add_with_child(out_placemark,'Point', 'coordinates', coordinates)
      ET.SubElement(out_placemark, 'description').text = 'Data retrieved from liveuamap.com'
      out_ext_data = ET.SubElement(out_placemark,'ExtendedData')
      if date:
        out_data = KH.add_with_child(out_ext_data, 'Data', 'value', date)
        out_data.attrib['name'] = 'date'
      for attribute, data in props.items():
        if attribute in SKIP_EXTENDED_DATA:
          continue
        # print(attribute,data)
        out_data = KH.add_with_child(out_ext_data, 'Data', 'value', str(data))
        out_data.attrib['name'] = attribute
      pic_list = picture
      if pics is not None:
        if pic_list is not None:
          pic_list += ' '
        pic_list += pics
      if pic_list is not None:
        # show = '>%s<\n' % pic_list
        # sys.stderr.write(show)
        gx_media = KH.add_with_child(out_ext_data, 'Data', 'value', pic_list)
        gx_media.attrib['name'] = 'gx_media_links'

  # # Finish by writing out the KML
  xmlstr = minidom.parseString(ET.tostring(out_kml)).toprettyxml(indent="   ")
  print(xmlstr)

def usage():
  return '''
    Converts GeoJSON from liveuamap.com to KML that can be imported to a google map.

    Usage: liveua_kml_for_google.py <-> | <FILE>
 
      Use - to read from STDIN
      Otherwise pass a filename as the only argument
  '''

def main():
  if len(sys.argv) != 2:
    sys.exit(usage())

  arg = sys.argv[1]
  if arg == '-':
    input_string = ''
    for line in fileinput.input():
      input_string += line
  else:
    kml_file = open(arg)
    input_string = kml_file.read()
    kml_file.close

  parseGeoJSON(input_string)

main()