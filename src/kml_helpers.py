import xml.etree.ElementTree as ET

KML_NAMESPACE = '{http://www.opengis.net/kml/2.2}'

# Gets the text from a child element
# example of XML we would use this on to get 'Kyiv': 
# <Data>
#     <value>Kyiv</value>
# </Data>
def get_child_text(element, child_name='value'):
  return element.find(KML_NAMESPACE + child_name).text

# Creates a new SubElement with its own child SubElement with the specified value
# example: add_with_child(element, 'Data', 'value', 'Kyiv') ->
# <Data>
#     <value>Kyiv</value>
# </Data>
def add_with_child(element, name, child_name, value):
  elem = ET.SubElement(element, name)
  ET.SubElement(elem, child_name).text = value
  return elem