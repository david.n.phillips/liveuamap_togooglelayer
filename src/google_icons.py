import xml.etree.ElementTree as ET
import re
import kml_helpers as KH

SYMB_CIRCLE = '1499'
SYMB_BOMB = '1564'


def icon_symbol_of_json(feature):
  # TODO use picpath?
  return SYMB_CIRCLE

def convert_color(rgb):
  if (rgb is None):
    return "000000"
  res = re.search(r"#(..)(..)(..)", rgb)
  return "%s%s%s" % (res.group(3),res.group(2),res.group(1))
