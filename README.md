# liveuamap_togooglelayer

## Name
liveua2googlemap

## Description
Convert GeoJSON data from liveuamap.com to KML that can be imported to a google map

The output from this tool can be imported to a google map.

Manual example:
 - Go to mymaps.google.com
 - Click on: CREATE A NEW MAP
 - The map will have an 'Untitled layer'. Click 'Import'
 - Click 'Select a file from your device'
 - Browse to the file from this repo: sample_data/geojson-export.kml
 - There will be an error message about columns.  That's just google being a whiner.  The import should have worked.

## Installation
### Add dependencies
sudo apt install python3-pip
sudo pip install timezonefinder



## Usage
 - Convert Liveuamap GeoJSON to KML
 ```
     cat sample_data/geojson-export.json | python3 src/liveua_kml_for_google.py -
```
## Support

## Roadmap

## Contributing

## Authors and acknowledgment
David Phillips - david.n.phillips@gmail.com

## License
This work is licensed under the Creative Commons Attribution-ShareAlike 2.0 Generic License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/2.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

## Project status
